
const get_datalayer = (val) =>{
    let final_val
    let event 
    let eventCategory
    let eventAction
    let eventLabel

    val.forEach(function(i){
        event = i["event"];      
        eventAction = i["eventAction"];      
        eventCategory = i["eventCategory"];
        eventLabel = i["eventLabel"];
        final_val = `${event}+${eventAction}+${eventCategory}+${eventLabel}`      
    });

    return final_val
}

const define_email = () => {
    let data = new Date();
    let dia = String(data.getDate()).padStart(2, '0');
    let mes = String(data.getMonth() + 1).padStart(2, '0');
    let ano = data.getFullYear();
    let hr = data.getHours();
    let sec = data.getSeconds();
    let minute = data.getMinutes();
    let dataAtual = dia + mes + ano + hr + minute + sec;
    let email = `qualidade+${dataAtual}@studiovisual.com.br`
    return email;
}

export {get_datalayer, define_email};