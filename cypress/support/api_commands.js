
Cypress.Commands.add('check_user_registration_on_api', (email_lead) =>{
    cy.wait(1000)
    cy.request({
        method: 'GET',
        url: `https://api.hubapi.com/contacts/v1/contact/email/${email_lead}/profile?hapikey=4cbbb363-74e2-497d-bead-c6e59fd41f4b`
    }).then(consult => {
        expect(consult.status).to.be.equal(200)
    })
})