
Cypress.Commands.add('close_popup', () =>{
    cy.get('#pmodalpopup > .modal-dialog > .modal-content > .modal-header > .close').click({ force: true });
})

Cypress.Commands.add('form_ligamos_para_voce', function(email_lead){
    cy.get('.btn-group > .btn').should('be.visible').click();
    cy.wait(1000)
    cy.fixture('UserData').as('user').then(() =>{
        cy.get('#firstname-971ce355-5482-43d9-97e1-8bfb72b1b2d7').should('be.visible').type('teste nao contatar ')
        cy.get('#email-971ce355-5482-43d9-97e1-8bfb72b1b2d7').type(email_lead)
        cy.get('#phone-971ce355-5482-43d9-97e1-8bfb72b1b2d7').type(this.user.tel)
        cy.get('#mobilephone-971ce355-5482-43d9-97e1-8bfb72b1b2d7').type(this.user.phone)
        cy.get('#cargo_geral-971ce355-5482-43d9-97e1-8bfb72b1b2d7').select(this.user.cargo)
        cy.get('#department-971ce355-5482-43d9-97e1-8bfb72b1b2d7').select(this.user.department)
        cy.get('#company-971ce355-5482-43d9-97e1-8bfb72b1b2d7').type(this.user.company)
        cy.get('#segmento_de_atuacao_geral-971ce355-5482-43d9-97e1-8bfb72b1b2d7').select(this.user.segmento)
        cy.get('#especialidade_educacional-971ce355-5482-43d9-97e1-8bfb72b1b2d7').select(this.user.subsegmento)
        cy.get('[for="faixa_de_faturamento_clone_0-971ce355-5482-43d9-97e1-8bfb72b1b2d7"]').should('be.visible').click()
        cy.get('.hs-form-booleancheckbox-display').click()
    })
})