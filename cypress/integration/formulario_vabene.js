describe('Página Formulário', () => {

    before(() => {
        cy.visit('https://vabene.com.br/seja-um-revendedor');
        
    });

    it('Validando datalayer no evento de clique do botão', () => {
        cy.get('#input_3_1').type('Nome da empresa');
        cy.get('#input_3_4').type('https://gravityforms.com');
        cy.get('#input_3_7').type('rodrigo@gmail.com');
        cy.get('#input_3_9').type('2199999999');
        cy.get('#input_3_11').select('Epi');
        cy.get('#input_3_2').type('11111111111111');
        cy.get('#input_3_6').type('Nome qualquer');
        cy.get('#input_3_8').select('Alagoas');
        cy.get('#input_3_8').type('2199999999');
        cy.get('#input_3_12').select('Só estou cotando');
        cy.get('#input_3_10').type('2137107809');
        cy.get('#label_3_14_1').click();
        cy.get('#input_3_13').type('Essa é uma mensagem de teste, vamos adicionar alguns textos e ver o que vai dar!!!!');
        cy.get('#label_3_15_1').click();
        cy.get('#gform_submit_button_3').click();
    });
});
